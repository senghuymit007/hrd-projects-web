// date and time of the bottombar
var myVar = setInterval(myTimer, 1000);

function myTimer() {
  var now = new Date();
  var date = now.toDateString();
  var time = now.toLocaleTimeString();

  document.getElementById("local_time").innerHTML = (date + " " + time);
}


let btnStart = document.getElementById('start-counter');
let btnStop = document.getElementById('stop-counter');
let btnClear = document.getElementById('clear-counter')
let textStart = document.getElementById('start')
let textCounter = document.getElementById('counter')
let real_per_minute = document.getElementById('real_per_minute')

let real = document.getElementById('real');

btnStop.style.display = "none"
btnClear.style.display = "none"

var set;
btnStart.onclick = () => {
  var pageVisisted = new Date();
  set = setInterval(function () {
    let hours = pageVisisted.getHours()
    var sminute = pageVisisted.getMinutes()
    let a_p = pageVisisted.toLocaleTimeString().split(" ")[1]
    textStart.innerHTML = hours + ":" + sminute + " " + a_p;
  }, 1000);

  btnStart.style.display = "none"
  btnStop.style.display = "block"
}

var oldTime = new Date();
var startMinute = oldTime.getTime();
var oldHour = oldTime.getHours();
var n = oldTime.getMinutes()

btnStop.onclick = () => {
  btnStop.style.display = "none"
  btnClear.style.display = "block"
  var newTime = new Date();
  let hours = newTime.getHours()
  let endM = newTime.getTime()
  let m = newTime.getMinutes();
  let a_p = newTime.toLocaleTimeString().split(" ")[1]
  let totalMinutes = endM - startMinute
  let total = Math.round(totalMinutes / 60000);
  textCounter.innerHTML = hours + ":" + m + " " + a_p;

  function timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + " hour(s) / " + rminutes ;
    }
    let totalMoney1 = timeConvert(total)
  real_per_minute.innerHTML = totalMoney1

  //test
  function timeConvert1(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    return rhours;
    }
    let totalTime1 = timeConvert1(total)
  //test

  //total money
  if ( total == 0 ) {
    real.innerHTML = "You have not use time yet!"
  } else if ( total < 16 ) {
    real.innerHTML = "500 Real(s)"
  } else if ( total < 31 ) {
    real.innerHTML = "1000 Real(s)"
  } else if ( total < 60 ) {
    real.innerHTML = "1500 Real(s)"
  } else if ( total >= 60 ) {
      real.innerHTML = (2000 * totalTime1) + " Real(s)" 
  }
}

btnClear.onclick = () => {
  clearInterval(set)
  window.location.reload()
}
